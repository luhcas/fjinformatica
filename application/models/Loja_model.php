<?php
class Loja_model extends CI_Model {
  public $table = 'clientes';

  function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function insert(array $data){
    return $this->db->insert($this->table, $data);
  }

  function fetch_clientes(){
    $this->db->order_by('id', 'ASC');
    $query = $this->db->get('clientes');
    return $query->result();
  }

  function fetch_cliente($id){
    $this->db->order_by('id', 'ASC');
    $this->db->where('id', $id);
    $query = $this->db->get('clientes');
    return $query->result();
  }

  function fetch_saldo($id){
    $this->db->select('saldo');
    $this->db->where('id', $id);
    $query = $this->db->get('clientes');
    return $query->result();
  }

  function fetch_relatorio(){
    $this->db->select('sum(saldo) as saldo, count(id) as quantidade');
    $query = $this->db->get('clientes');
    return $query->result();
  }

}
?>
