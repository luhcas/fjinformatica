<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
  * @author Lucas AC <lucas_alves_lbas@hotmail.com>
  * @copyright 2019 - JL TECNO - Soluções para Web <http://jltecno.com.br>
**/
class Fale extends CI_Controller {
	public function __construct()
	{
    parent::__construct();
	}

	public function index()
	{
    //output('fale-conosco');
		$this->load->view('fale');
	}
}
