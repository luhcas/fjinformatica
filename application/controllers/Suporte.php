<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
  * @author Lucas AC <lucas_alves_lbas@hotmail.com>
  * @copyright 2019 - JL TECNO - Soluções para Web <http://jltecno.com.br>
**/
class Suporte extends CI_Controller {
	public function __construct()
	{
    parent::__construct();
	}

	public function index()
	{
		$this->load->view('suporte');
	}
}
