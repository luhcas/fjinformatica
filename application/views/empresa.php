<?php $this->load->view('elements/header');?>
<?php $this->load->view('elements/sidebar');?>
<section id="empresa">
  <div class="container text-justify">
    <br><br><br><br>
    <div class="col-md-8">
      <h4>Nossa história</h4>
      <hr>
      Sou um parágrafo. Clique aqui para me editar e adicionar seu texto. É fácil! Basta clicar em "Editar Texto" ou clicar duas vezes sobre mim e você poderá adicionar seu conteúdo e trocar fontes. Você pode arrastar e soltar-me em qualquer lugar de sua página. Sou um ótimo lugar para contar sua história e permitir que seus visitantes saibam um pouco mais sobre você.
      <br><br> Este é um ótimo espaço para escrever um texto longo sobre sua empresa e seus serviços. Você pode usar este espaço para entrar em detalhes sobre sua empresa. Fale sobre a sua equipe e sobre os serviços prestados por você. Conte aos seus visitantes sobre como teve a ideia de iniciar o seu negócio e o que o torna diferente de seus concorrentes. Faça com que sua empresa se destaque e mostre quem você é.
      <br><br> No Wix, amamos criar templates que lhe permitem construir sites incríveis! Tudo isso graças ao feedback e apoio de usuários como você! Mantenha-se atualizado sobre novos recursos na seção Sobre Wix, na página de Suporte. Sinta-se à vontade para dar sua opinião e feedback no Fórum Wix Responde. Se quiser se beneficiar de um toque de um designer profissional, vá ao Arena Wix e contate um dos designers Wix Pro. Ou, se precisar de mais ajuda, pode digitar suas perguntas no Fórum de Suporte e receber respostas imediatas. Para estar sempre atualizado com tudo que acontece no Wix, incluindo dicas e coisas legais, entre no Wix Blog! Dica: Adicione a sua própria imagem clicando duas vezes sobre a imagem e em Trocar imagem.
      <br><br>
      Sou um parágrafo. Clique aqui para me editar e adicionar seu texto. É fácil! Basta clicar em "Editar Texto" ou clicar duas vezes sobre mim e você poderá adicionar seu conteúdo e trocar fontes. Você pode arrastar e soltar-me em qualquer lugar de sua página. Sou um ótimo lugar para contar sua história e permitir que seus visitantes saibam um pouco mais sobre você.
      <br><br> Este é um ótimo espaço para escrever um texto longo sobre sua empresa e seus serviços. Você pode usar este espaço para entrar em detalhes sobre sua empresa. Fale sobre a sua equipe e sobre os serviços prestados por você. Conte aos seus visitantes sobre como teve a ideia de iniciar o seu negócio e o que o torna diferente de seus concorrentes. Faça com que sua empresa se destaque e mostre quem você é.
      <br><br> No Wix, amamos criar templates que lhe permitem construir sites incríveis! Tudo isso graças ao feedback e apoio de usuários como você! Mantenha-se atualizado sobre novos recursos na seção Sobre Wix, na página de Suporte. Sinta-se à vontade para dar sua opinião e feedback no Fórum Wix Responde. Se quiser se beneficiar de um toque de um designer profissional, vá ao Arena Wix e contate um dos designers Wix Pro. Ou, se precisar de mais ajuda, pode digitar suas perguntas no Fórum de Suporte e receber respostas imediatas. Para estar sempre atualizado com tudo que acontece no Wix, incluindo dicas e coisas legais, entre no Wix Blog! Dica: Adicione a sua própria imagem clicando duas vezes sobre a imagem e em Trocar imagem.
    </div>
    <div class="col-md-4">
      <br><br>
      <div class="borda">
        <img src="<?php echo base_url(); ?>assets/img/lampada.webp" class="imagem_empresa" alt="">
      </div>
      <br>
      <div class="borda">
        <img src="<?php echo base_url(); ?>assets/img/reuniao.webp" class="imagem_empresa" alt="">
      </div>
    </div>
  </div>
  <br><br>
</section>
<?php $this->load->view('elements/footer');?>
