<?php $this->load->view('elements/header');?>
<?php $this->load->view('elements/sidebar');?>
<section id="fale_conosco">
  <div class="container text-justify">
    <br><br><br><br>
    <div class="col-md-12">
      <h4>Fale conosco</h4>
      <hr>
      <h6>Vamos conversar</h6>
​      Agradecemos seu interesse por nossos serviços. Por favor preencha o formulário abaixo, envie-o e entraremos em contato contigo em breve.
      <br><br>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-7">
          <form class="" action="index.html" method="post">
            <div class="form-group">
              <input type="text" class="form-control" name="nome" value="" placeholder="Nome">
              <input type="text" class="form-control" name="email" value="" placeholder="E-mail">
              <input type="text" class="form-control" name="assunto" value="" placeholder="Assunto">
              <textarea name="name" class="form-control" rows="6" cols="80" placeholder="Mensagem"></textarea>
            </div>
            <input type="submit" class="btn" name="" value="Enviar" style="float: right; background-color: #313538; color: #fff;">
          </form>
        </div>
        <div class="col-md-5">
          <div class="borda_cinza">
            <img src="<?php echo base_url(); ?>assets/img/teclado_dois.webp" class="imagem_empresa" alt="">
          </div>
        </div>
      </div>
    </div>
  </div>
  <br><br>
</section>

<?php $this->load->view('elements/footer');?>
