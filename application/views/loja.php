<?php $this->load->view('elements/header');?>
<?php $this->load->view('elements/sidebar');?>

<section id="um">
<hr class="primeiro_hr">
<div class="container">
  <div class="col-md-5 col-sm-12 tablet white">
    <br>
    <h1>Soluções de TI seguras para um ambiente seguro</h1>
    <p class="texto">A Tecnologia da Informação em seu contexto geral é uma ferramenta que vem ganhando cada vez mais notoriedade no ambiente corporativo da atualidade. É reconhecida como arma estratégica competitiva, pois, além de permitir e sustentar as operações do negócio viabiliza novas estratégias empresariais.</p>
    <button type="button" name="button" class="primeiro_botao">SAIBA MAIS</button>
  </div>
  <div class="col-md-6 col-sm-12">
    <img src="<?php echo base_url(); ?>assets/img/primeira.webp" class="primera_imagem hidden-sm-down" alt="">
    <img src="<?php echo base_url(); ?>assets/img/segunda.webp" class="segunda_imagem hidden-sm-down" alt="">
  </div>
</div>
<hr class="segundo_hr">
</section>
<section id="produtos">
  <div class="container">
    <br><br><br>
    <div class="col-md-4 col-sm-12 col-xs-12 servicos borda_cerrilada">
      <img src="<?php echo base_url(); ?>assets/img/camera.webp" class="imagem_produto" alt="">
      <h5>Segurança</h5>
      <hr>
      <p class="texto">Mantenha seu imóvel monitorado 24h e acesse tudo do celular, notebook, computador ou tablet de onde você estiver. Filmagens de alta qualidade seja de dia ou de noite, com captação de audio.</p>
      <button type="button" name="button" class="segundo_botao">SAIBA MAIS</button>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12 servicos borda_cerrilada">
      <img src="<?php echo base_url(); ?>assets/img/notebook.webp" class="imagem_produto" alt="">
      <h5>Manutenção</h5>
      <hr>
      <p class="texto">Mantenha seu computador livre de vírus, seus equipamentos atualizados e de agilidade ao seu trabalho. Trabalhamos com concertos de computadores, notebooks, impressoras, nobreaks...</p>
      <button type="button" name="button" class="segundo_botao">SAIBA MAIS</button>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12 servicos">
      <img src="<?php echo base_url(); ?>assets/img/servidor.webp" class="imagem_produto servidor" alt="">
      <h5>Servidores</h5>
      <hr>
      <p class="texto">Deixe sua rede segura, prática e estável, com rotinas de backups, opções de trabalhar em sua casa via acesso remoto, infraestrutura adequada ao ambiente, restrições de acesso ao usuário, ...</p>
      <button type="button" name="button" class="segundo_botao">SAIBA MAIS</button>
    </div>
  </div>
  <br>
</section>
<section id="banner">
  <div class="container">
    <div class="row">
      <br>
      <div class="col-md-12 quadro">
        <div class="col-md-6">
          <br><br>
          <img src="<?php echo base_url(); ?>assets/img/banner.jpg" alt="" class="hidden-xs-down img_banner">
          <img src="<?php echo base_url(); ?>assets/img/banner_xs.jpg" alt="" class="hidden-sm-up img_banner_xs">
        </div>
        <div class="col-md-6 text-justify white">
          <br><br>
          <h5>Atendimento remoto <br>
            Resolva seu problema agora mesmo !
          </h5>

          <br>
          <p class="pezinho">Através da internet, a FJ Informática acessa os computadores de seus clientes e soluciona problemas de maneira rápida e segura. O cliente acompanha pela tela de seu computador o que está sendo executado pela nossa equipe de acesso remoto, verifica como realizamos as soluções, confirma a correção e funcionamento da operação e volta a utilizar o equipamento em poucos minutos...</p>
          <button type="button" name="button" class="terceiro_botao float_right">SAIBA MAIS</button><br><br>
        </div>
      </div>
      <br><br>
    </div>
  </div>
</section>
<section id="quatro">
  <div class="container">
    <br><br>
    <div class="hidden-sm-up">
      <br><br><br><br><br><br><br>
    </div>
    <h5 class="text-centered">Parceiro de TI com uma visão compartilhada.</h5>
    <div class="row">
      <div class="col-sm-12 col-xs-12 hidden-sm-up titulos_xs">
        <br>
        <h5>Mobilidade</h5>
      </div>
      <div class="col-md-3 col-xs-4 float-md-left float-xs-right text-right">
        <br><span class="hidden-sm-down">Mobilidade</span><br>
        <img src="<?php echo base_url(); ?>assets/img/celular_cinza.webp" id="celular" alt="">
      </div>
      <div class="col-md-9 text-justify">
        <br>
        <b class="hidden-sm-down">Mobilidade</b> <br> Sou um parágrafo. Clique aqui para me editar e adicionar seu texto. É fácil! Basta clicar em "Editar Texto" ou clicar duas vezes sobre mim e você poderá adicionar seu conteúdo e trocar fontes. Você pode arrastar e soltar-me em qualquer lugar de sua página. Sou um ótimo lugar para contar sua história e permitir que seus visitantes saibam um pouco mais sobre você.
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 col-xs-12 hidden-sm-up titulos_xs">
        <br>
        <h5>Computação de nuvem</h5>
      </div>
      <div class="col-md-3 float-md-left float-xs-right text-right">
        <br><span class="hidden-sm-down">Computação de nuvem</span><br>
        <img src="<?php echo base_url(); ?>assets/img/nuvemzinha.webp" class="imagem_produto nuvemzinha" alt="">
        <img src="<?php echo base_url(); ?>assets/img/pasta.webp" class="imagem_produto pasta" alt="">
      </div>
      <div class="col-md-9 text-justify">
        <br>
        <b class="hidden-sm-down">Computação de nuvem</b> <br> Este é um ótimo espaço para escrever um texto longo sobre sua empresa e seus serviços. Você pode usar este espaço para entrar em detalhes sobre sua empresa. Fale sobre a sua equipe e sobre os serviços prestados por você. Conte aos seus visitantes sobre como teve a ideia de iniciar o seu negócio e o que o torna diferente de seus concorrentes. Faça com que sua empresa se destaque e mostre quem você é.
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 col-xs-12 hidden-sm-up titulos_xs">
        <br>
        <h5>Segurança</h5>
      </div>
      <div class="col-md-3 float-md-left float-xs-right text-right">
        <br> <span class="hidden-sm-down">Segurança</span>  <br>
        <img src="<?php echo base_url(); ?>assets/img/computador_cinza.webp" id="computador" class="imagem_produto" alt="">
      </div>
      <div class="col-md-9 text-justify">
        <br>
        <b class="hidden-sm-down">Segurança</b> <br> No Wix, amamos criar templates que lhe permitem construir sites incríveis! Tudo isso graças ao feedback e apoio de usuários como você! Mantenha-se atualizado sobre novos recursos na seção Sobre Wix, na página de Suporte. Sinta-se à vontade para dar sua opinião e feedback no Fórum Wix Responde. Se quiser se beneficiar de um toque de um designer profissional, vá ao Arena Wix e contate um dos designers Wix Pro. Ou, se precisar de mais ajuda, pode digitar suas perguntas no Fórum de Suporte e receber respostas imediatas. Para estar sempre atualizado com tudo que acontece no Wix, incluindo dicas e coisas legais, entre no Wix Blog!
      </div>
    </div>
    <br>
  </div>
</section>
<?php $this->load->view('elements/footer');?>
