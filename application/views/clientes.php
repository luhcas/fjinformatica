<?php $this->load->view('elements/header');?>
<?php $this->load->view('elements/sidebar');?>
<section id="clientes">
  <div class="container text-justify">
    <br><br><br><br>
    <div class="col-md-12">
      <h4>Clientes</h4>
      <hr>
      <p>Sou um parágrafo. Clique aqui para me editar e adicionar seu texto. É fácil! Basta clicar em "Editar Texto" ou clicar duas vezes sobre mim e você poderá adicionar seu conteúdo e trocar fontes. Você pode arrastar e soltar-me em qualquer lugar de sua página. Sou um ótimo lugar para contar sua história e permitir que seus visitantes saibam um pouco mais sobre você.</p>
    </div>

    <div class="col-md-12 text-centered">
      <div class="row">
        <div class="col-md-3" class="quadroFotos" style="padding-left:0; padding-right:0;">
          <a href="#" data-toggle="modal" data-target="#myModal">
            <figure class="figure_clientes">
              <img src="<?php echo base_url(); ?>assets/img/um.webp" class="foto_clientes" alt="">
              <figcaption class="descricao">
                <div class="col-md-12 text-centered">
                  <h4>Nome da empresa</h4>
                  <hr>
                  <h5>Descrição</h5>
                </div>
              </figcaption>
            </figure>
          </a>
        </div>
        <div class="col-md-3" class="quadroFotos"  style="padding-left:0; padding-right:0;">
          <a href="#" data-toggle="modal" data-target="#myModal">
            <figure class="figure_clientes">
              <img src="<?php echo base_url(); ?>assets/img/dois.webp" class="foto_clientes" alt="">
              <figcaption class="descricao">
                <div class="col-md-12 text-centered">
                  <h4>Nome da empresa</h4>
                  <hr>
                  <h5>Descrição</h5>
                </div>
              </figcaption>
            </figure>
          </a>
        </div>
        <div class="col-md-3" class="quadroFotos" style="padding-left:0; padding-right:0;">
          <a href="#" data-toggle="modal" data-target="#myModal">
            <figure class="figure_clientes">
              <img src="<?php echo base_url(); ?>assets/img/tres.webp" class="foto_clientes" alt="">
              <figcaption class="descricao">
                <div class="col-md-12 text-centered">
                  <h4>Nome da empresa</h4>
                  <hr>
                  <h5>Descrição</h5>
                </div>
              </figcaption>
            </figure>
          </a>
        </div>
        <div class="col-md-3" style="padding-left:0; padding-right:0;">
          <a href="#" data-toggle="modal" data-target="#myModal">
            <figure class="figure_clientes">
                  <img src="<?php echo base_url(); ?>assets/img/quatro.webp" class="foto_clientes" alt="">
              <figcaption class="descricao">
                <div class="col-md-12 text-centered">
                  <h4>Nome da empresa</h4>
                  <hr>
                  <h5>Descrição</h5>
                </div>
              </figcaption>
            </figure>
          </a>
        </div>
      </div>
    </div>

    <div class="col-md-12 text-centered">
      <div class="row">
        <div class="col-md-3" class="quadroFotos" style="padding-left:0; padding-right:0;">
          <a href="#" data-toggle="modal" data-target="#myModal">
            <figure class="figure_clientes">
              <img src="<?php echo base_url(); ?>assets/img/quatro.webp" class="foto_clientes" alt="">
              <figcaption class="descricao">
                <div class="col-md-12 text-centered">
                  <h4>Nome da empresa</h4>
                  <hr>
                  <h5>Descrição</h5>
                </div>
              </figcaption>
            </figure>
          </a>
        </div>
        <div class="col-md-3" class="quadroFotos"  style="padding-left:0; padding-right:0;">
          <a href="#" data-toggle="modal" data-target="#myModal">
            <figure class="figure_clientes">
              <img src="<?php echo base_url(); ?>assets/img/tres.webp" class="foto_clientes" alt="">
              <figcaption class="descricao">
                <div class="col-md-12 text-centered">
                  <h4>Nome da empresa</h4>
                  <hr>
                  <h5>Descrição</h5>
                </div>
              </figcaption>
            </figure>
          </a>
        </div>
        <div class="col-md-3" class="quadroFotos" style="padding-left:0; padding-right:0;">
          <a href="#" data-toggle="modal" data-target="#myModal">
            <figure class="figure_clientes">
              <img src="<?php echo base_url(); ?>assets/img/dois.webp" class="foto_clientes" alt="">
              <figcaption class="descricao">
                <div class="col-md-12 text-centered">
                  <h4>Nome da empresa</h4>
                  <hr>
                  <h5>Descrição</h5>
                </div>
              </figcaption>
            </figure>
          </a>
        </div>
        <div class="col-md-3" style="padding-left:0; padding-right:0;">
          <a href="#" data-toggle="modal" data-target="#myModal">
            <figure class="figure_clientes">
                  <img src="<?php echo base_url(); ?>assets/img/um.webp" class="foto_clientes" alt="">
              <figcaption class="descricao">
                <div class="col-md-12 text-centered">
                  <h4>Nome da empresa</h4>
                  <hr>
                  <h5>Descrição</h5>
                </div>
              </figcaption>
            </figure>
          </a>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <br>
      <h4>Depoimentos</h4>
      <br>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="<?php echo base_url(); ?>assets/img/um.webp" class="imagem_depoimento" alt="...">
            <div class="depoimento">
              “Sou um depoimento. Clique aqui para me editar e adicionar um texto que conte algo bem legal sobre você e seus serviços. Deixe que seus visitantes avaliem e indiquem você para seus amigos."
  ​​            <br>Samanta Moura, Gerente de Projeto
            </div>
          </div>
          <div class="carousel-item">
            <img src="<?php echo base_url(); ?>assets/img/dois.webp" class="imagem_depoimento" alt="...">
            <div class="depoimento">
              “Sou um depoimento. Clique aqui para me editar e adicionar um texto que conte algo bem legal sobre você e seus serviços. Deixe que seus visitantes avaliem e indiquem você para seus amigos."
  ​​            <br>Samanta Moura, Gerente de Projeto
            </div>
          </div>
          <div class="carousel-item">
            <img src="<?php echo base_url(); ?>assets/img/tres.webp" class="imagem_depoimento" alt="...">
            <div class="depoimento">
              “Sou um depoimento. Clique aqui para me editar e adicionar um texto que conte algo bem legal sobre você e seus serviços. Deixe que seus visitantes avaliem e indiquem você para seus amigos."
  ​​            <br>Samanta Moura, Gerente de Projeto
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br><br>
</section>
<?php $this->load->view('elements/footer');?>
