<?php $this->load->view('elements/header');?>
<?php $this->load->view('elements/sidebar');?>
<section id="servicos">
  <div class="container text-justify">
    <br><br><br><br>
    <div class="col-md-12">
      <h4>Serviços</h4>
      <hr>
      <p>Sou um parágrafo. Clique aqui para me editar e adicionar seu texto. É fácil! Basta clicar em "Editar Texto" ou clicar duas vezes sobre mim e você poderá adicionar seu conteúdo e trocar fontes. Você pode arrastar e soltar-me em qualquer lugar de sua página. Sou um ótimo lugar para contar sua história e permitir que seus visitantes saibam um pouco mais sobre você.</p>
    </div>
    <div class="col-md-12">
      <div class="col-md-4 col-sm-12 col-xs-12 caixa margin_right_5">
        <div class="col-md-12 caixa_azul">
          <br>
          <h6>Sou um título.​</h6>
        </div>
        <div class="col-md-12 padding_zero">
          <img src="<?php echo base_url(); ?>assets/img/mao.webp" class="imagem_servico" alt="">
        </div>
        <div class="col-md-12 padding_zero text-justify"><br>
          Sou um parágrafo. Clique aqui para me editar e adicionar seu texto. É fácil! Basta clicar em "Editar Texto" ou clicar duas vezes sobre mim e você poderá adicionar seu conteúdo e trocar fontes. Você pode arrastar e soltar-me em qualquer lugar de sua página. Sou um ótimo lugar para contar sua história e permitir que seus visitantes saibam um pouco mais sobre você.
          Dica: Use esta área para descrever um de seus serviços. Você pode trocar o título para o serviço que você oferece e usar esta área de texto para descrever seu serviço.
          <br><br>
        </div>
      </div>
      <div class="col-md-4 col-sm-12 col-xs-12 caixa margin_right_5 margin_left_5">
        <div class="col-md-12 caixa_azul">
          <br>
          <h6>Sou um título.​</h6>
        </div>
        <div class="col-md-12 padding_zero">
          <img src="<?php echo base_url(); ?>assets/img/cabo.webp" class="imagem_servico" alt="">
        </div>
        <div class="col-md-12 padding_zero text-justify"><br>
          Sou um parágrafo. Clique aqui para me editar e adicionar seu texto. É fácil! Basta clicar em "Editar Texto" ou clicar duas vezes sobre mim e você poderá adicionar seu conteúdo e trocar fontes. Você pode arrastar e soltar-me em qualquer lugar de sua página. Sou um ótimo lugar para contar sua história e permitir que seus visitantes saibam um pouco mais sobre você.
          Dica: Use esta área para descrever um de seus serviços. Você pode trocar o título para o serviço que você oferece e usar esta área de texto para descrever seu serviço.
          <br><br>
        </div>
      </div>
      <div class="col-md-4 col-sm-12 col-xs-12 caixa margin_left_5">
        <div class="col-md-12 caixa_azul">
          <br>
          <h6>Sou um título.​</h6>
        </div>
        <div class="col-md-12 padding_zero">
          <img src="<?php echo base_url(); ?>assets/img/teclado.webp" class="imagem_servico" alt="">
        </div>
        <div class="col-md-12 padding_zero text-justify"><br>
          Sou um parágrafo. Clique aqui para me editar e adicionar seu texto. É fácil! Basta clicar em "Editar Texto" ou clicar duas vezes sobre mim e você poderá adicionar seu conteúdo e trocar fontes. Você pode arrastar e soltar-me em qualquer lugar de sua página. Sou um ótimo lugar para contar sua história e permitir que seus visitantes saibam um pouco mais sobre você.
          Dica: Use esta área para descrever um de seus serviços. Você pode trocar o título para o serviço que você oferece e usar esta área de texto para descrever seu serviço.
          <br><br>
        </div>
      </div>
    </div>
  </div>
  <br><br>
</section>

<?php $this->load->view('elements/footer');?>
