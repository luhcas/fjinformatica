<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>FJ Informática</title>
		<base href="{sys_site_url}">
		<meta name="description" content="Prestador de serviços de TI, manutenção, suporte, rede, impressoras, alarmes , câmeras de segurança CFTV, alarmes e vendas">

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="twitter:card" value="summary">
		<meta name="twitter:title" content="Passagem">
		<meta name="twitter:description" content="">
		<meta name="twitter:image" content="http://assets/img/logo-social.png">

		<meta property="og:title" content="Passagem" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="http://" />
		<meta property="og:image" content="http://assets/img/logo-social.png'" />
		<meta property="og:description" content="" />
		<meta property="og:site_name" content="" />

		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon.png" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.png" type="image/x-icon">

		<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet">
		<!--<link href="<//?php echo base_url(); ?>assets/css/estilo.css" rel="stylesheet">-->

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
		<link href="<?php echo base_url(); ?>assets/css/estilo.css" rel="stylesheet">
	</head>
<body>

<?php function moeda($valor){
	echo number_format($valor, 2, ',', '.');
} ?>
