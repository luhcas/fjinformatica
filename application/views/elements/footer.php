<footer>
  <div class="container">
    <br>
    <div class="row">
      <div class="col-md-2">
        <a href="#" class="a_footer">Sobre a empresa</a><br>
        <hr class="hr_footer">
        <a href="#" class="a_footer">Serviços</a><br>
        <hr class="hr_footer">
        <a href="#" class="a_footer">Clientes</a><br>
        <hr class="hr_footer">
        <a href="#" class="a_footer">Carreira</a><br>
        <hr class="hr_footer">
      </div>
      <div class="col-md-1"></div>
      <div class="col-md-2">
        <a href="#" class="a_footer">Suporte</a><br>
        <hr class="hr_footer">
        <a href="#" class="a_footer">Últimas notícias</a><br>
        <hr class="hr_footer">
        <a href="#" class="a_footer">Nossos clientes</a><br>
        <hr class="hr_footer">
      </div>
      <div class="col-md-1"></div>
      <div class="col-md-2">
        <a href="#" class="a_footer">Siga-nos no twitter</a><br>
        <hr class="hr_footer">
        <a href="#" class="a_footer">Curta-nos no facebook</a><br>
        <hr class="hr_footer">
        <a href="#" class="a_footer">Faça parte de nossa equipe</a><br>
        <hr class="hr_footer">
        <a href="#" class="a_footer">Dê-nos seu feedback</a><br>
        <hr class="hr_footer">
      </div>
      <div class="col-md-3">

      </div>
    </div>
    <div class="row">
      <br>
      <div class="col-md-12">
        <img src="<?php echo base_url(); ?>assets/img/facebook.webp" alt="">
        <img src="<?php echo base_url(); ?>assets/img/twitter.webp" alt="">
        <img src="<?php echo base_url(); ?>assets/img/google_mais.webp" alt="">
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 rodape">
        © 2015 Todos os direitos reservados FJ Informática - Site ativo desde 2011 <br>
        É proibida a reprodução do conteúdo deste site, em qualquer meio, eletrônico ou impresso, sem autorização escrita.
      </div>
    </div>
  </div>
  <br>
</footer>
</html>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.1.0.js"></script>
<script src="<?php echo base_url(); ?>assets/js/tether.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery.mask.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url(); ?>assets/js/valida_cpf_cnpj.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.maskMoney.min.js"></script>
