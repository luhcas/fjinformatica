<style media="screen">

</style>
<?php $paginaCorrente = basename($_SERVER['REQUEST_URI']);?>
<header>
<!-- Barra de navegação -->
<nav class="navbar navbar-fixed-top navbar-light bg-light navheader">

    <!--<img src="assets/img/logomarca_mini.png" alt="logotipo SICEO" style="width: 183px; height:auto;" class=" d-inline-block align-top">-->

  <button class="navbar-toggler hidden-md-up float-sm-right float-xs-right" type="button" data-toggle="collapse" data-target="#menu-sanduiche"></button>
  <div class="collapse navbar-toggleable-sm" id="menu-sanduiche">
    <ul class="nav navbar-nav float-md-right">
      <li class="nav-item">
        <br class="hidden-sm-up">
        <a href="<?php echo base_url(); ?>" <?php if($paginaCorrente != 'empresa' && $paginaCorrente != 'servicos' && $paginaCorrente != 'produtos' && $paginaCorrente != 'clientes' && $paginaCorrente != 'suporte' && $paginaCorrente != 'fale') {echo 'class="nav-link corrente"';}else{echo 'class="nav-link"';} ?>>Página inicial</a>
      </li>
      <li class="nav-item">
        <a href="<?php echo base_url(); ?>index.php/empresa" <?php if($paginaCorrente == 'empresa') {echo 'class="nav-link corrente"';}else{echo 'class="nav-link"';} ?>>Sobre a empresa</a>
      </li>
      <li class="nav-item">
        <a href="<?php echo base_url(); ?>index.php/servicos" <?php if($paginaCorrente == 'servicos') {echo 'class="nav-link corrente"';}else{echo 'class="nav-link"';} ?>>Serviços</a>
      </li>
      <li class="nav-item">
        <a href="<?php echo base_url(); ?>index.php/produtos" <?php if($paginaCorrente == 'produtos') {echo 'class="nav-link corrente"';}else{echo 'class="nav-link"';} ?>>Produtos</a>
      </li>
      <li class="nav-item">
        <a href="<?php echo base_url(); ?>index.php/clientes" <?php if($paginaCorrente == 'clientes') {echo 'class="nav-link corrente"';}else{echo 'class="nav-link"';} ?>>Clientes</a>
      </li>
      <li class="nav-item">
        <a href="<?php echo base_url(); ?>index.php/suporte" <?php if($paginaCorrente == 'suporte') {echo 'class="nav-link corrente"';}else{echo 'class="nav-link"';} ?>>Suporte</a>
      </li>
      <li class="nav-item">
        <a href="<?php echo base_url(); ?>index.php/fale" <?php if($paginaCorrente == 'fale') {echo 'class="nav-link corrente"';}else{echo 'class="nav-link"';} ?>>Fale conosco</a>
      </li>
      <li class="nav-item hidden-xs-down">&nbsp;</li>
    </ul>
  </div>
</nav>
<!-- Fim - Barra de navegação -->

</header>
<br><br><br><br>
<div class="container conteudo_fixo">
  <div class="col-md-6 col-sm-12 col-xs-12">
    <img src="<?php echo base_url(); ?>assets/img/logo.webp" id="logo" alt="">
  </div>
  <div class="col-md-6 col-sm-12 col-xs-12">
    <img src="<?php echo base_url(); ?>assets/img/telefone.webp" alt=""> 11.2442-9159 <br>
    <div class="hidden-xs-down">
      <img src="<?php echo base_url(); ?>assets/img/email.webp" class="" alt=""> atendimento@fjinformatica.net
    </div>
    <div class="row hidden-sm-up">
      <img src="<?php echo base_url(); ?>assets/img/email.webp" class="" alt=""> atendimento@fjinformatica.net
    </div>
  </div>
</div>
